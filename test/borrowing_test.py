from flask import Flask, jsonify, request
from mysql.connector import Error
import json
import sys
sys.path.append('../model')
from databases import createDatabases
from datetime import timedelta
import datetime
import time
import isoweek

app = Flask(__name__)
connection = createDatabases()
#print(connection)


@app.route('/api/v1/borrower', methods=['POST'])
def createBorrower():
    body = request.json
    Borrow_ID = body['Borrow_ID']
    Book_ID = body['Book_ID']
    User_ID	= body['User_ID']
    status = body['status']
    limit = 5
    
    Borrow_Date = datetime.datetime.now()
    borrow_day = Borrow_Date.isoweekday()
    if borrow_day == 1 or 2 or 3 or 4:
        return_date = Borrow_Date + datetime.timedelta(days=8)
        Return_Date = return_date.strftime("%Y-%m-%d %H:%M:%S")
    elif borrow_day == 5:
        return_date = Borrow_Date + datetime.timedelta(days=10)
        Return_Date = return_date.strftime("%Y-%m-%d %H:%M:%S")
    else:
        return "Close"
    
    cursor = connection.cursor(dictionary=True)
    try:
        mysql_get_query1 = "select distinct* from borrowwer where Borrow_ID = {};".format(Borrow_ID)
        #"""SELECT borrowwer.Book_ID AS borrowwer, book.Book_ID AS book FROM borrowwer LEFT JOIN book ON borrowwer.Book_ID = book.Book_Name"""
        cursor.execute(mysql_get_query1)
        result_1 = cursor.fetchall()
        print(result_1)
        for item in result_1:
            print(item)
        
        if len(result_1) < 5:
            mysql_insert_querty = "INSERT INTO borrowwer(Borrow_ID, Book_ID, User_ID, Borrow_Date, Return_Date, status) values('{}', '{}', '{}', '{}', '{}', '{}');"\
                .format(Borrow_ID, Book_ID, User_ID, Borrow_Date, Return_Date, status)
            result_2 = cursor.execute(mysql_insert_querty)
            print(result_2)
            connection.commit()
        else:
            print("การยืมถึงจำนวนสูงสุดไม่สามารถยืมได้")
    except Exception as e:
        print(e)
        connection.rollback()
        connection.close()
        return jsonify({"status":401})  
    return jsonify({"status":200})




@app.route('/api/v1/borrower', methods=['PUT'])
def updateBorrower():
    body = request.json
    Borrow_ID = body['Borrow_ID']
    Book_ID = body['Book_ID']
    User_ID	= body['User_ID']
    Borrow_Date = body['Borrow_Date']
    Return_Date	= body['Return_Date']
    actual_date = body['actual_date']	
    status = body['status']

    cursor = connection.cursor(dictionary=True)
    mysql_get_time1 = "select distinct Return_Date , actual_date from borrowwer where Borrow_ID = '{}';".format(Borrow_ID)
    cursor.execute(mysql_get_time1)
    result_time = cursor.fetchall()
    print(type(result_time))
    print(result_time)
    
    try:
        mysql_update_time1 = "UPDATE borrowwer SET actual_date = '{}' WHERE Borrow_ID = '{}';".format(actual_date, Borrow_ID)
        actual_update = cursor.execute(mysql_update_time1)
        connection.commit()
    except Exception as e:
        print(e)
        return jsonify({"status":400})

    date_time_actual = datetime.datetime.strptime(actual_date, '%Y-%m-%d %H:%M:%S')
    print(type(date_time_actual))
    date_time_return = datetime.datetime.strptime(Return_Date, '%Y-%m-%d %H:%M:%S')
    print(type(date_time_return))
    timediff = date_time_actual - date_time_return
    over_date = timediff.days
    print(over_date)

    try:
        mysql_update_time2 = "UPDATE borrowwer SET over_date = '{}' WHERE Borrow_ID = '{}';".format(over_date, Borrow_ID)
        over_update = cursor.execute(mysql_update_time2)
        connection.commit()
    except Exception as e:
        connection.rollback()
        connection.close()
        print(e)
        return jsonify({"status":401})
    return jsonify({"status":200})


@app.route('/api/v1/borrower/<string:User_ID>/<string:Borrow_ID>', methods=['GET','DELETE'])
def getAndDeleteBorrower(User_ID,Borrow_ID):
    cursor = connection.cursor(dictionary=True)
    try:
        mysql_get_query = "SELECT * FROM borrowwer WHere User_ID = '{}' and Borrow_ID = '{}' ;".format(User_ID, Borrow_ID)
        cursor.execute(mysql_get_query,)
        result_1 = cursor.fetchall()
        return jsonify(result_1)
        if request.json == 'GET':
            mysql_get_query = "SELECT * FROM borrowwer WHere User_ID = '{}' and Borrow_ID = '{}' ;".format(User_ID, Borrow_ID)
            cursor.execute(mysql_get_query,)
            result_1 = cursor.fetchall()
            return jsonify(result_1)
        if request.method == 'DELETE':
            mysql_delete_query = "DELETE FROM borrowwer WHERE User_ID = '{}' and Borrow_ID = '{}' ;".format(User_ID, Borrow_ID)
            result2 = cursor.execute(mysql_delete_query,)
            connection.commit()
            return jsonify({"message":"methods delete success"})
        else:
            if request.method == 'GET':
                return jsonify({"message" : "Not found"})
    except Exception as e:
        connection.rollback()
        connection.close()
        print(e)
        return jsonify({"status":400})
    

    





if __name__=="__main__":
    app.run(host='localhost', port='5000')